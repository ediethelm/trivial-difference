(uiop:define-package #:trivial-difference
  (:documentation "")
  (:use #:common-lisp)
  (:export #:determine-difference
	   #:difference-operator
	   #:operation
	   #:new-value
	   #:old-value))

