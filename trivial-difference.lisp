;;;; Copyright (c) Eric Diethelm 2019 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license.


(in-package #:trivial-difference)

(defclass difference-operator ()
  ((operation :reader operation
	      :initarg :operation
	      :type (member :keep :add :remove :replace))
   (new :reader new-value
	:initarg :new)
   (old :reader old-value
	:initarg :old)))

(defun should-keep? (lhs rhs)
  (mapcar #'(lambda (elm) (if (member elm rhs :test #'trivial-utilities:equals)
			       (list :probable-keep elm)
			       elm))
	  lhs))

(defun evaluate-difference (new old &optional (result '()))
  (when (and (null new) (null old))
    (return-from evaluate-difference
      (if result
	  (reverse result)
	  (list (make-instance 'difference-operator :operation :keep :new nil :old nil)))))
  
  (let ((new-value (car new))
	(old-value (car old))
	(next-new (cdr new))
	(next-old (cdr old)))
    (push (cond ((or (and (null new-value) (null old-value))
		     (trivial-utilities:equals new-value old-value))
		 (let ((lhs (if (and (consp new-value) (eq (car new-value) :probable-keep)) (cadr new-value) new-value))
		       (rhs (if (and (consp old-value) (eq (car old-value) :probable-keep)) (cadr old-value) old-value)))
		   (make-instance 'difference-operator :operation :keep :new lhs :old rhs)))
		((null new-value)
		 (make-instance 'difference-operator :operation :remove :old old-value :new nil))
		((null old-value)
		 (make-instance 'difference-operator :operation :add :old nil :new new-value))
		((and (consp old-value)
		      (eq (car old-value) :probable-keep))
		 (setf next-old old)
		 (make-instance 'difference-operator :operation :add :new new-value :old nil))
		((and (consp new-value)
		      (eq (car new-value) :probable-keep))
		 (setf next-new new)
		 (make-instance 'difference-operator :operation :remove :new nil :old old-value))
		(t
		 (make-instance 'difference-operator :operation :replace :new new-value :old old-value)))
	  result)

    (evaluate-difference next-new next-old result)))

(defgeneric determine-difference (new old)
  (:documentation ""))

(defmethod determine-difference ((new t) (old t))
  (list (make-instance 'difference-operator
		       :new new
		       :old old
		       :operation (cond ((trivial-utilities:equals new old)
					 :keep)
					((null new)
					 :remove)
					((null old)
					 :add)
					(t
					 :replace)))))

(defmethod determine-difference ((new list) (old list))
  (let ((new (should-keep? new old))
	(old (should-keep? old new)))

    (evaluate-difference new old)))

