;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :trivial-difference
  :name "trivial-difference"
  :description ""
  :version "0.0.4"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-utilities)
  :in-order-to ((test-op (test-op :trivial-difference/test)))
  :components ((:file "package")
	       (:file "trivial-difference")))

(defsystem :trivial-difference/test
  :name "trivial-difference/test"
  :description "Unit Tests for the trivial-difference project."
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-difference fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :trivial-difference-tests))
  :components ((:file "test-trivial-difference")))

