# Trivial Difference Manual

###### \[in package TRIVIAL-DIFFERENCE\]
[![pipeline status](https://gitlab.com/ediethelm/trivial-difference/badges/master/pipeline.svg)](https://gitlab.com/ediethelm/trivial-difference/commits/master)
[![Quicklisp](http://quickdocs.org/badge/trivial-difference.svg)](http://quickdocs.org/trivial-difference/)

## Description


## Installing trivial-difference

This project is available in the latest [QuickLisp](https://www.quicklisp.org/beta/ "QuickLisp") distribution, so installing it is reduced to calling:

```lisp
(ql:quickload :trivial-difference)
```

But if you want access to the latest updates, install it by cloning the Git repository with

```bash
cd $HOME/quicklisp/local-projects
git clone https://gitlab.com/ediethelm/trivial-difference.git
```

and then loading it as usual via [QuickLisp](https://www.quicklisp.org/beta/ "QuickLisp") as above.

## Working Example


## Exported Symbols


## License Information

This library is released under the MIT License. Please refer to the [LICENSE](LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](CONTRIBUTING.md "Contributing") document for more information.

