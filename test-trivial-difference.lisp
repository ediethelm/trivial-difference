
(in-package :trivial-difference)

(5am:def-suite :trivial-difference-tests :description "Trivial Difference tests")
(5am:in-suite :trivial-difference-tests)

(defun difference-contains (difference-result operation new-value old-value)
  (find-if #'(lambda (x) (and (eq (operation x) operation)
			      (or (and (null (old-value x)) (null old-value))
				  (trivial-utilities:equals (old-value x) old-value))
			      (or (and (null (new-value x)) (null new-value))
				  (trivial-utilities:equals (new-value x) new-value))))
	   difference-result))

(5am:test single-element
  ;; (difference 7 nil)
  ;; Expected -> ((:add 7 nil))
  (let ((result (determine-difference 7 nil)))
    (5am:is-true (difference-contains result :add 7 nil))
    (5am:is (eq (length result) 1))))

  
(5am:test list-difference
  ;; (difference '() '(7))
  ;; Expected -> ((:remove nil 7))
  (let ((result (determine-difference '() '(7))))
    (5am:is-true (difference-contains result :remove nil 7))
    (5am:is (eq (length result) 1)))

  ;; (difference '(2) '())
  ;; Expected -> ((:add 2 nil))
  (let ((result (determine-difference '(2) '())))
    (5am:is-true (difference-contains result :add 2 nil))
    (5am:is (eq (length result) 1)))
  
  ;; (difference '(1 2 3 4 5) '(4 5 8 13))
  ;; Expected -> ((:add 1) (:add 2) (:add 3) (:keep 4) (:remove 8) (:remove 13))
  (let ((result (determine-difference '(1 2 3 4 5) '(4 5 8 13))))
    (5am:is-true (difference-contains result :add 1 nil))
    (5am:is-true (difference-contains result :add 2 nil))
    (5am:is-true (difference-contains result :add 3 nil))
    (5am:is-true (difference-contains result :keep 4 4))
    (5am:is-true (difference-contains result :keep 5 5))
    (5am:is-true (difference-contains result :remove nil 8))
    (5am:is-true (difference-contains result :remove nil 13))
    (5am:is (eq (length result) 7)))
  
  ;; (difference '(1 2 3 7) '(1 3 4 7))
  ;; Expected -> ((:keep 1) (:add 2) (:keep 3) (:remove 4) (:keep 7))
  (let ((result (determine-difference '(1 2 3 7) '(1 3 4 7))))
    (5am:is-true (difference-contains result :keep 1 1))
    (5am:is-true (difference-contains result :add 2 nil))
    (5am:is-true (difference-contains result :keep 3 3))
    (5am:is-true (difference-contains result :remove nil 4))
    (5am:is-true (difference-contains result :keep 7 7))
    (5am:is (eq (length result) 5)))

  ;; (difference '(1 2 3 7) '(1 5 3 7))
  ;; Expected -> ((:keep 1) (:replace 5 2) (:keep 3) (:keep 7))

  (let ((result (determine-difference '(1 2 3 7) '(1 5 3 7))))
    (5am:is-true (difference-contains result :keep 1 1))
    (5am:is-true (difference-contains result :replace 2 5))
    (5am:is-true (difference-contains result :keep 3 3))
    (5am:is-true (difference-contains result :keep 7 7))
    (5am:is (eq (length result) 4))))
